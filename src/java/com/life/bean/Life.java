package com.life.bean;

import com.life.classes.Counter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Aleksandr
 */
@ManagedBean (name = "life")
@SessionScoped
public class Life{
    private int xSize = 5;
    private int ySize = 5;
    private int kolForLife = 3; //сколько рядом клеток нужно для зарождения жизни
    private Integer[] kolSosedForLife = {new Integer(2), new Integer(3)}; // число соседей для не умерания клетки
    private Boolean[][] mass;
    private List<List<Boolean>> output;
    
    
    @PostConstruct
    public void init(){
        render();
    }
    
    public void settings(){
        render();
    }
    
    public void nextStep(){
        int kol = 0;
        Counter c = new Counter();
        Boolean[][] newMass = new Boolean[ySize][xSize];
        for (int i = 0; i < ySize; i++) {
            for (int j = 0; j < xSize; j++) {

                //Определение числа живых соседей
                if (i == 0) {
                    if (j == 0) {
                        kol = c.leftUp(mass);
                    } else if (j == xSize - 1) {
                        kol = c.rightUp(mass, j);
                    } else {
                        kol = c.iUpBorder(mass, j);
                    }
                } else if (i == ySize - 1) {
                    if (j == 0) {
                        kol = c.leftDown(mass, i);
                    } else if (j == xSize - 1) {
                        kol = c.rightDown(mass, i, j);
                    } else {
                        kol = c.iDownBorder(mass, i, j);
                    }
                } else {
                    if (j == 0) {
                        kol = c.jLeftBorder(mass, i);
                    } else if (j == xSize - 1) {
                        kol = c.jRightBorder(mass, i, j);
                    } else {
                        kol = c.notInBorder(mass, i, j);
                    }
                }

                // действия при результатах
                if (mass[i][j]) {//клетка жива
                    if (Arrays.asList(kolSosedForLife).contains(new Integer(kol))) {
                        newMass[i][j] = true;
                    } else {
                        newMass[i][j] = false;
                    }
                } else { // клетка мертва
                    if (kol == kolForLife) {
                        newMass[i][j] = true;
                    } else {
                        newMass[i][j] = false;
                    }
                }
            }
        }

        mass = newMass;
        convert();
    }
    
    private void render(){
        mass = new Boolean[ySize][xSize];
        for (int i = 0; i < ySize; i++) {
            for (int j = 0; j < xSize; j++) {
                mass[i][j] = false;
            }
        }
        convert();
    }
    
    private void convert(){
        output = new ArrayList<List<Boolean>>() ;
        for (Boolean[] b :mass){
            output.add(Arrays.asList(b));
        }
    }
    
    public void change(int i, int j){
        if (mass[i][j])
            mass[i][j] = false;
        else
            mass[i][j] = true;
        convert();
    }
    
    
    public int getxSize() {
        return xSize;
    }

    public void setxSize(int xSize) {
        this.xSize = xSize;
    }

    public int getySize() {
        return ySize;
    }

    public void setySize(int ySize) {
        this.ySize = ySize;
    }

    public int getKolForLife() {
        return kolForLife;
    }

    public void setKolForLife(int kolForLife) {
        this.kolForLife = kolForLife;
    }

    public Integer[] getKolSosedForLife() {
        return kolSosedForLife;
    }

    public void setKolSosedForLife(Integer[] kolSosedForLife) {
        this.kolSosedForLife = kolSosedForLife;
    }

    public List<List<Boolean>> getOutput() {
        return output;
    }

    public void setOutput(List<List<Boolean>> output) {
        this.output = output;
    }

    
}
