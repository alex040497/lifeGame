/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.life.classes;

/**
 *
 * @author Aleksandr
 */
public class Counter {
    
    public int notInBorder(Boolean[][] mass, int i, int j){
        int count = 0;
        if (mass[i-1][j-1])
            count++;
        if (mass[i-1][j])
            count++;
        if (mass[i-1][j+1])
            count++;
        if (mass[i+1][j-1])
            count++;
        if (mass[i+1][j])
            count++;
        if (mass[i+1][j+1])
            count++;
        if (mass[i][j-1])
            count++;
        if (mass[i][j+1])
            count++;
        return count;
    }
    
    public int iUpBorder(Boolean[][] mass, int j){
        int count = 0;
        if (mass[0][j-1])
            count++;
        if (mass[0][j+1])
            count++;
        if (mass[1][j-1])
            count++;
        if (mass[1][j])
            count++;
        if (mass[1][j+1])
            count++;
        return count;
    }
    
    public int jLeftBorder(Boolean[][] mass, int i){
        int count = 0;
        if (mass[i-1][0])
            count++;
        if (mass[i+1][0])
            count++;
        if (mass[i-1][1])
            count++;
        if (mass[i+1][1])
            count++;
        if (mass[i][1])
            count++;
        return count;
    }
    
    public int iDownBorder(Boolean[][] mass, int i, int j){
        int count = 0;
        if (mass[i][j-1])
            count++;
        if (mass[i][j+1])
            count++;
        if (mass[i-1][j-1])
            count++;
        if (mass[i-1][j])
            count++;
        if (mass[i-1][j+1])
            count++;
        return count;
    }
    
    public int jRightBorder(Boolean[][] mass, int i, int j){
        int count = 0;
        if (mass[i-1][j-1])
            count++;
        if (mass[i-1][j])
            count++;
        if (mass[i][j-1])
            count++;
        if (mass[i+1][j-1])
            count++;
        if (mass[i+1][j])
            count++;
        return count;
    }
    
    public int leftUp(Boolean[][] mass){
        int count = 0;
        if (mass[0][1])
            count++;
        if (mass[1][0])
            count++;
        if (mass[1][1])
            count++;
        return count;
    }
    
    public int rightUp(Boolean[][] mass, int j){
        int count = 0;
        if (mass[0][j-1])
            count++;
        if (mass[1][j-1])
            count++;
        if (mass[1][j])
            count++;
        return count;
    }
    public int leftDown(Boolean[][] mass, int i){
        int count = 0;
        if (mass[i-1][0])
            count++;
        if (mass[i-1][1])
            count++;
        if (mass[i][1])
            count++;
        return count;
    }
    public int rightDown(Boolean[][] mass, int i, int j){
        int count = 0;
        if (mass[i-1][j-1])
            count++;
        if (mass[i][j-1])
            count++;
        if (mass[i-1][j])
            count++;
        return count;
    }
}
